package bootstrap

import (
	"net/http"
	"strconv"
	"sync"

	"apiCompaniesList/dto"
	"apiCompaniesList/repositories"
	"apiCompaniesList/tools"

	"github.com/go-chi/chi/v5"
)

type HttpServer struct {
	Config dto.ConfigDto
}

func (httpServer *HttpServer) Init() {
	requestMutex := &sync.Mutex{}
	config := httpServer.Config

	fileHandler := tools.FileHandlerImplementation{
		RequestMutex: requestMutex,
		Config:       config,
	}
	repository := repositories.CompaniesRepositoryFile{FileHandler: &fileHandler}
	jsonHandler := tools.JsonHandlerImplementation{}

	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		companies, err := repository.Get()
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}

		response, err := jsonHandler.ConvertListCompaniesDto(companies)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}

		w.WriteHeader(200)
		w.Write(response)
	})

	r.With(httpServer.SecurityMiddleware).Get("/getCompaniesInfo", func(w http.ResponseWriter, r *http.Request) {
		companies, err := repository.Get()
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}

		response, err := jsonHandler.ConvertListCompaniesDto(companies)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}

		w.WriteHeader(200)
		w.Write(response)
	})

	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		name := r.FormValue("company")
		taxNumber, _ := strconv.ParseInt(r.FormValue("taxNumber"), 10, 64)
		phone := r.FormValue("phone")
		address := r.FormValue("address")
		owner := r.FormValue("owner")

		company := dto.CompanyDto{
			Name:      name,
			TaxNumber: taxNumber,
			Phone:     phone,
			Address:   address,
			OwnerName: owner,
		}

		err := repository.Add(company)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}
	})

	r.Delete("/", func(w http.ResponseWriter, r *http.Request) {
		name := r.FormValue("company")
		taxNumber, _ := strconv.ParseInt(r.FormValue("taxNumber"), 10, 64)

		company := dto.CompanyDto{
			Name:      name,
			TaxNumber: taxNumber,
		}

		err := repository.Delete(company)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
		}
	})

	http.ListenAndServe(":80", r)
}

func (httpServer *HttpServer) SecurityMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		configBearer := "Bearer " + httpServer.Config.Bearer

		requestBearer := r.Header.Get("Authorization")

		if configBearer != requestBearer {
			w.WriteHeader(401)
			w.Write([]byte("Bad token."))
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
