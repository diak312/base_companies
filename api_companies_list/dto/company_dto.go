package dto

type CompanyDto struct {
	Name      string
	TaxNumber int64
	Phone     string
	Address   string
	OwnerName string
}
