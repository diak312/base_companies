package tools

import "apiCompaniesList/dto"

type FileHandler interface {
	AddCompany(company dto.CompanyDto) error
	GetListCompanies() ([]dto.CompanyDto, error)
	DeleteByName(name string) error
	DeleteByTaxNumber(taxNumber int64) error
}
