package tools

import (
	"encoding/csv"
	"errors"
	"os"
	"strconv"
	"sync"

	"apiCompaniesList/dto"
)

type FileHandlerImplementation struct {
	RequestMutex *sync.Mutex
	Config       dto.ConfigDto
}

func (fileHandler *FileHandlerImplementation) AddCompany(company dto.CompanyDto) error {
	companies, err := fileHandler.getListCompanies()
	if err != nil {
		return err
	}

	for _, existCompany := range companies {
		if existCompany == company {
			return errors.New("This company is already in the database.")
		}
	}

	fileHandler.RequestMutex.Lock()

	recordFile, err := os.OpenFile(fileHandler.Config.Fileaddress, os.O_APPEND|os.O_RDWR, os.ModeAppend)
	if err != nil {
		return err
	}
	writer := csv.NewWriter(recordFile)
	var csvData = []string{
		company.Name,
		strconv.FormatInt(company.TaxNumber, 10),
		company.Phone,
		company.Address,
		company.OwnerName,
	}

	err = writer.Write(csvData)
	if err != nil {
		return err
	}

	writer.Flush()

	fileHandler.RequestMutex.Unlock()

	return nil
}

func (fileHandler *FileHandlerImplementation) GetListCompanies() ([]dto.CompanyDto, error) {
	companies, err := fileHandler.getListCompanies()
	if err != nil {
		return []dto.CompanyDto{}, err
	}

	return companies, nil
}

func (fileHandler *FileHandlerImplementation) DeleteByName(name string) error {
	companies, err := fileHandler.getListCompanies()
	if err != nil {
		return err
	}

	fileHandler.RequestMutex.Lock()

	recordFile, err := os.OpenFile(fileHandler.Config.Fileaddress, os.O_APPEND|os.O_RDWR, os.ModeAppend)
	writer := csv.NewWriter(recordFile)

	newCompanies := [][]string{}
	for _, company := range companies {
		if company.Name != name {
			newCompanies = append(newCompanies, []string{
				company.Name,
				strconv.FormatInt(company.TaxNumber, 10),
				company.Phone,
				company.Address,
				company.OwnerName,
			})
		}
	}

	recordFile.Truncate(0)
	err = writer.WriteAll(newCompanies)
	if err != nil {
		return err
	}

	writer.Flush()

	fileHandler.RequestMutex.Unlock()

	return nil
}

func (fileHandler *FileHandlerImplementation) DeleteByTaxNumber(taxNumber int64) error {
	companies, err := fileHandler.getListCompanies()
	if err != nil {
		return err
	}

	fileHandler.RequestMutex.Lock()

	recordFile, err := os.OpenFile(fileHandler.Config.Fileaddress, os.O_APPEND|os.O_RDWR, os.ModeAppend)
	writer := csv.NewWriter(recordFile)

	newCompanies := [][]string{}
	for _, company := range companies {
		if company.TaxNumber != taxNumber {
			newCompanies = append(newCompanies, []string{
				company.Name,
				strconv.FormatInt(company.TaxNumber, 10),
				company.Phone,
				company.Address,
				company.OwnerName,
			})
		}
	}

	recordFile.Truncate(0)
	err = writer.WriteAll(newCompanies)
	if err != nil {
		return err
	}

	writer.Flush()

	fileHandler.RequestMutex.Unlock()

	return nil
}

func (fileHandler *FileHandlerImplementation) getListCompanies() ([]dto.CompanyDto, error) {
	fileHandler.RequestMutex.Lock()

	recordFile, err := os.Open(fileHandler.Config.Fileaddress)
	if err != nil {
		return []dto.CompanyDto{}, err
	}

	reader := csv.NewReader(recordFile)

	companies := []dto.CompanyDto{}

	for companyRecord, err := reader.Read(); err == nil; companyRecord, err = reader.Read() {
		if err != nil {
			return []dto.CompanyDto{}, err
		}

		taxNumber, _ := strconv.ParseInt(companyRecord[1], 10, 64)

		companies = append(companies, dto.CompanyDto{
			Name:      companyRecord[0],
			TaxNumber: taxNumber,
			Phone:     companyRecord[2],
			Address:   companyRecord[3],
			OwnerName: companyRecord[4],
		})
	}

	fileHandler.RequestMutex.Unlock()

	return companies, nil
}
