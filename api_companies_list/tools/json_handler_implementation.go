package tools

import (
	"encoding/json"

	"apiCompaniesList/dto"
)

type JsonHandlerImplementation struct {
}

func (jsonHandler *JsonHandlerImplementation) ConvertListCompaniesDto(companies []dto.CompanyDto) ([]byte, error) {
	companiesString, err := json.Marshal(companies)
	if err != nil {
		return nil, err
	}

	return companiesString, nil
}
