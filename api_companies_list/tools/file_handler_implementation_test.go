package tools

import (
	"bufio"
	"encoding/csv"
	"os"
	"strconv"
	"strings"
	"sync"
	"testing"

	"apiCompaniesList/dto"
)

func TestAddCompany(t *testing.T) {
	t.Parallel()

	var wg sync.WaitGroup
	countTestIteration := 1000
	mutex := &sync.Mutex{}
	config := dto.ConfigDto{
		Fileaddress: "../asserts/companies_add_test.csv",
	}

	err := createTestFile(config)
	if err != nil {
		t.Error(err)
	}

	fhr := FileHandlerImplementation{
		RequestMutex: mutex,
		Config:       config,
	}

	wg.Add(countTestIteration)

	for i := 0; i < countTestIteration; i++ {
		go func(iteration int) {
			err := fhr.AddCompany(
				dto.CompanyDto{
					Name:      strings.Join([]string{"test", strconv.Itoa(iteration)}, ""),
					TaxNumber: int64(iteration),
					Phone:     strings.Join([]string{"phone", strconv.Itoa(iteration)}, ""),
					Address:   strings.Join([]string{"address", strconv.Itoa(iteration)}, ""),
					OwnerName: strings.Join([]string{"owner", strconv.Itoa(iteration)}, ""),
				},
			)
			if err != nil {
				t.Error(err)
			}

			wg.Done()
		}(i)
	}
	wg.Wait()

	countLines := lineCounter(config)

	if countTestIteration != countLines {
		t.Error("Invalid number of lines!")
	}

	defer deleteTestFile(config)
}

func TestGetListCompanies(t *testing.T) {
	t.Parallel()

	var wg sync.WaitGroup
	countTestIteration := 10
	mutex := &sync.Mutex{}
	config := dto.ConfigDto{
		Fileaddress: "../asserts/companies_get_test.csv",
	}

	err := createTestFile(config)
	if err != nil {
		t.Error(err)
	}

	err = fillingFile(config)
	if err != nil {
		t.Error(err)
	}

	fhr := FileHandlerImplementation{
		RequestMutex: mutex,
		Config:       config,
	}

	etalonList := []dto.CompanyDto{
		{
			Name:      "test1",
			TaxNumber: 1,
			Phone:     "phone1",
			Address:   "address1",
			OwnerName: "owner1",
		},
	}

	wg.Add(countTestIteration)

	for i := 0; i < countTestIteration; i++ {
		go func() {
			list, err := fhr.GetListCompanies()
			if err != nil {
				t.Error(err)
			}

			if etalonList[0] != list[0] {
				t.Error("Wrong value received!")
			}

			wg.Done()
		}()
	}
	wg.Wait()

	defer deleteTestFile(config)
}

func TestDeleteByName(t *testing.T) {
	t.Parallel()

	var wg sync.WaitGroup
	mutex := &sync.Mutex{}
	config := dto.ConfigDto{
		Fileaddress: "../asserts/companies_delete_by_name_test.csv",
	}

	err := createTestFile(config)
	if err != nil {
		t.Error(err)
	}

	err = fillingFile(config)
	if err != nil {
		t.Error(err)
	}

	fhr := FileHandlerImplementation{
		RequestMutex: mutex,
		Config:       config,
	}

	wg.Add(2)

	go func() {
		err := fhr.DeleteByName("test2")
		if err != nil {
			t.Error(err)
		}

		wg.Done()
	}()

	go func() {
		err := fhr.DeleteByName("test3")
		if err != nil {
			t.Error(err)
		}

		wg.Done()
	}()

	list, err := fhr.getListCompanies()
	if err != nil {
		t.Error(err)
	}

	etalon := dto.CompanyDto{Name: "test1", TaxNumber: 1, Phone: "phone1", Address: "address1", OwnerName: "owner1"}

	if list[0] != etalon {
		t.Error("Invalid deletion result")
	}

	wg.Wait()

	defer deleteTestFile(config)
}

func TestDeleteByTaxNumber(t *testing.T) {
	t.Parallel()

	var wg sync.WaitGroup
	mutex := &sync.Mutex{}
	config := dto.ConfigDto{
		Fileaddress: "../asserts/companies_delete_by_tax_number_test.csv",
	}

	err := createTestFile(config)
	if err != nil {
		t.Error(err)
	}

	err = fillingFile(config)
	if err != nil {
		t.Error(err)
	}

	fhr := FileHandlerImplementation{
		RequestMutex: mutex,
		Config:       config,
	}

	wg.Add(2)

	go func() {
		err := fhr.DeleteByTaxNumber(2)
		if err != nil {
			t.Error(err)
		}

		wg.Done()
	}()

	go func() {
		err := fhr.DeleteByTaxNumber(3)
		if err != nil {
			t.Error(err)
		}

		wg.Done()
	}()

	list, err := fhr.getListCompanies()
	if err != nil {
		t.Error(err)
	}

	etalon := dto.CompanyDto{Name: "test1", TaxNumber: 1, Phone: "phone1", Address: "address1", OwnerName: "owner1"}

	if list[0] != etalon {
		t.Error("Invalid deletion result")
	}

	wg.Wait()

	defer deleteTestFile(config)
}

func createTestFile(config dto.ConfigDto) error {
	_, err := os.Create(config.Fileaddress)
	if err != nil {
		return err
	}

	return nil
}

func fillingFile(config dto.ConfigDto) error {
	recordFile, err := os.OpenFile(config.Fileaddress, os.O_APPEND|os.O_RDWR, os.ModeAppend)
	if err != nil {
		return err
	}

	writer := csv.NewWriter(recordFile)
	var csvData = [][]string{
		{"test1", "1", "phone1", "address1", "owner1"},
		{"test2", "2", "phone2", "address2", "owner2"},
		{"test3", "3", "phone3", "address3", "owner3"},
	}

	err = writer.WriteAll(csvData)
	if err != nil {
		return err
	}

	writer.Flush()

	return nil
}

func deleteTestFile(config dto.ConfigDto) error {
	err := os.Remove(config.Fileaddress)
	if err != nil {
		return err
	}

	return nil
}

func lineCounter(config dto.ConfigDto) int {
	file, _ := os.Open(config.Fileaddress)
	fileScanner := bufio.NewScanner(file)
	lineCount := 0

	for fileScanner.Scan() {
		lineCount++
	}

	return lineCount
}
