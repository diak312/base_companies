package tools

import "apiCompaniesList/dto"

type JsonHandler interface {
	ConvertListCompaniesDto(companies []dto.CompanyDto) ([]byte, error)
}
