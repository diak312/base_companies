package repositories

import "apiCompaniesList/dto"

type CompaniesRepository interface {
	Get() ([]dto.CompanyDto, error)
	Add(company dto.CompanyDto) error
	Delete(company dto.CompanyDto) error
}
