package repositories

import (
	"errors"

	"apiCompaniesList/dto"
	"apiCompaniesList/tools"
)

type CompaniesRepositoryFile struct {
	FileHandler tools.FileHandler
}

func (repository *CompaniesRepositoryFile) Get() ([]dto.CompanyDto, error) {
	list, err := repository.FileHandler.GetListCompanies()
	if err != nil {
		return []dto.CompanyDto{}, err
	}

	return list, nil
}

func (repository *CompaniesRepositoryFile) Add(company dto.CompanyDto) error {
	err := repository.FileHandler.AddCompany(company)
	if err != nil {
		return err
	}

	return nil
}

func (repository *CompaniesRepositoryFile) Delete(company dto.CompanyDto) error {
	if company.Name != "" {
		repository.FileHandler.DeleteByName(company.Name)
	} else if company.TaxNumber != 0 {
		repository.FileHandler.DeleteByTaxNumber(company.TaxNumber)
	} else {
		return errors.New("No data for deletion!")
	}

	return nil
}
