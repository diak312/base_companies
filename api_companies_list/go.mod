module apiCompaniesList

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.1
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
)
