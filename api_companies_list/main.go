package main

import (
	bootstrap "apiCompaniesList/internal"
	"fmt"
)

func main() {
	config, err := bootstrap.InitConfig()
	if err != nil {
		fmt.Println(err)
	}

	httpServer := bootstrap.HttpServer{Config: config}
	httpServer.Init()
}
