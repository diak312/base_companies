package dto

type CompanyDto struct {
	Id        int
	Name      string `json:"Name"`
	TaxNumber int64  `json:"TaxNumber"`
	Phone     string `json:"Phone"`
	Address   string `json:"Address"`
	OwnerName string `json:"OwnerName"`
}
