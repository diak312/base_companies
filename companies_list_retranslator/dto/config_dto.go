package dto

type ConfigDto struct {
	Dbconnection           string
	Dbmigrationsconnection string
	Urlapi                 string
	Requestfrequencyapi    int
	Bearer                 string
}
