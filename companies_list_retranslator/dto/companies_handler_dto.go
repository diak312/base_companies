package dto

type CompaniesHandlerDto struct {
	AddableCompanies   []CompanyDto
	DeletableCompanies []int
}
