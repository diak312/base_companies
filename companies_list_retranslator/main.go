package main

import (
	bootstrap "companiesListRetranslator/internal"
	"fmt"
)

func main() {
	interrogator := bootstrap.Interrogator{}
	err := interrogator.Init()
	if err != nil {
		fmt.Println(err)
	}
}
