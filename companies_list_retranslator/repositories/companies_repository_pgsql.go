package repositories

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"companiesListRetranslator/dto"

	"github.com/jackc/pgx/v4/pgxpool"
)

type CompaniesRepositoryPgsql struct {
	PoolConnections *pgxpool.Pool
}

func (repository *CompaniesRepositoryPgsql) GetList() ([]dto.CompanyDto, error) {
	companies := []dto.CompanyDto{}
	queryString := "SELECT id, name_company, tax_number, phone, address, owner FROM companies"

	conn, err := repository.PoolConnections.Acquire(context.Background())
	if err != nil {
		fmt.Printf("Unable to acquire a database connection: %v\n", err)
	}

	rows, err := conn.Conn().Query(context.Background(), queryString)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		company := dto.CompanyDto{}
		rows.Scan(&company.Id, &company.Name, &company.TaxNumber, &company.Phone, &company.Address, &company.OwnerName)
		companies = append(companies, company)
	}

	conn.Release()

	return companies, nil
}

func (repository *CompaniesRepositoryPgsql) AddList(companies []dto.CompanyDto) error {
	stringValues := convertCompanyDtoToString(companies)

	baseQuery := "INSERT INTO companies (name_company, tax_number, phone, address, owner) VALUES $1"
	query := strings.Replace(baseQuery, "$1", stringValues, -1)

	conn, err := repository.PoolConnections.Acquire(context.Background())
	if err != nil {
		return err
	}

	connection := conn.Conn()

	_, err = connection.Exec(context.Background(), query)

	if err != nil {
		return err
	}

	conn.Release()

	return nil
}

func (repository *CompaniesRepositoryPgsql) DeleteByListId(companiesId []int) error {
	stringCompaniesId := strings.Trim(strings.Replace(fmt.Sprint(companiesId), " ", ",", -1), "[]")
	baseQuery := "DELETE FROM companies WHERE id IN ($1)"
	query := strings.Replace(baseQuery, "$1", stringCompaniesId, -1)

	conn, err := repository.PoolConnections.Acquire(context.Background())
	if err != nil {
		return err
	}

	connection := conn.Conn()

	_, err = connection.Exec(context.Background(), query)

	if err != nil {
		return err
	}

	conn.Release()

	return nil
}

func convertCompanyDtoToString(companies []dto.CompanyDto) string {
	var companiesString string

	for _, company := range companies {
		companiesString = companiesString + " ('" +
			company.Name + "'," +
			strconv.FormatInt(company.TaxNumber, 10) + ",'" +
			company.Phone + "','" + company.Address + "','" +
			company.OwnerName + "'),"
	}

	companiesString = companiesString[:len(companiesString)-1]

	return companiesString
}
