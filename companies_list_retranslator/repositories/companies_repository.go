package repositories

import "companiesListRetranslator/dto"

type CompaniesRepository interface {
	GetList() ([]dto.CompanyDto, error)
	AddList(companies []dto.CompanyDto) error
	DeleteByListId(companies []int) error
}
