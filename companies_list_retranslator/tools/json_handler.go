package tools

import "companiesListRetranslator/dto"

type JsonHandler interface {
	ConvertToCompanyDto(bytesCompanies []byte) ([]dto.CompanyDto, error)
}
