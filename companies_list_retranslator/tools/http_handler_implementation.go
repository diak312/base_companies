package tools

import (
	"errors"
	"io/ioutil"
	"net/http"

	"companiesListRetranslator/dto"
)

type HttpHandlerImplementation struct {
	JsonConvertor JsonHandler
}

func (httpHandler *HttpHandlerImplementation) GetCurrentCompanies(config dto.ConfigDto) ([]dto.CompanyDto, error) {
	var bearer = "Bearer " + config.Bearer

	req, err := http.NewRequest("GET", config.Urlapi, nil)
	req.Header.Add("Authorization", bearer)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return []dto.CompanyDto{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil || resp.StatusCode != 200 {
		if len(body) > 0 {
			bodyToString := func(c []byte) string {
				n := 0
				for _, b := range c {
					if b == 0 {
						continue
					}
					c[n] = b
					n++
				}
				return string(c[:n])
			}
			errString := bodyToString(body)
			return []dto.CompanyDto{}, errors.New(errString)
		}

		return []dto.CompanyDto{}, err
	}

	companies, err := httpHandler.JsonConvertor.ConvertToCompanyDto(body)
	if err != nil {
		return []dto.CompanyDto{}, err
	}

	return companies, nil
}
