package tools

import "companiesListRetranslator/dto"

type HttpHandler interface {
	GetCurrentCompanies(config dto.ConfigDto) ([]dto.CompanyDto, error)
}
