package tools

import "companiesListRetranslator/dto"

type CompaniesHandler interface {
	CompareCompanies(requestedCompanies []dto.CompanyDto, dbCompanies []dto.CompanyDto) (dto.CompaniesHandlerDto, error)
}
