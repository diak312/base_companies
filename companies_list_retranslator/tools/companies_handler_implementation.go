package tools

import (
	"companiesListRetranslator/dto"
)

type CompaniesHandlerImplementation struct {
}

func (companiesHandler *CompaniesHandlerImplementation) CompareCompanies(requestedCompanies []dto.CompanyDto, dbCompanies []dto.CompanyDto) (dto.CompaniesHandlerDto, error) {
	addableCompanies := []dto.CompanyDto{}
	delatableCompanies := dbCompanies
	resultHandlCompanies := dto.CompaniesHandlerDto{}

	listEqualeDbKeys := []int{}
	var isAddable bool

	for _, requestCompany := range requestedCompanies {
		isAddable = true
		for dbKey, dbCompany := range dbCompanies {
			if companiesDtoIsEquale(requestCompany, dbCompany) {
				listEqualeDbKeys = append(listEqualeDbKeys, dbKey)
				delatableCompanies[dbKey] = dto.CompanyDto{}
				isAddable = false
				break
			}
		}
		if isAddable {
			addableCompanies = append(addableCompanies, requestCompany)
		}
	}

	resultHandlCompanies.AddableCompanies = addableCompanies
	resultHandlCompanies.DeletableCompanies = getDeletableKeys(delatableCompanies)

	return resultHandlCompanies, nil
}

func companiesDtoIsEquale(firstDto dto.CompanyDto, secondeDto dto.CompanyDto) bool {
	if firstDto.Name != secondeDto.Name {
		return false
	}

	if firstDto.TaxNumber != secondeDto.TaxNumber {
		return false
	}

	if firstDto.Phone != secondeDto.Phone {
		return false
	}

	if firstDto.Address != secondeDto.Address {
		return false
	}

	if firstDto.OwnerName != secondeDto.OwnerName {
		return false
	}

	return true
}

func getDeletableKeys(listCompanies []dto.CompanyDto) []int {
	listDelatableKeys := []int{}

	for _, company := range listCompanies {
		if company.Id != 0 {
			listDelatableKeys = append(listDelatableKeys, company.Id)
		}
	}

	return listDelatableKeys
}
