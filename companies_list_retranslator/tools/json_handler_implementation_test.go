package tools

import (
	"companiesListRetranslator/dto"
	"testing"
)

func TestConvertToCompanyDto(t *testing.T) {
	t.Parallel()

	jsonListCompanies := "[{\"Name\":\"test1\",\"TaxNumber\":1111111,\"Phone\":\"1111111\",\"Address\":\"addres1\",\"OwnerName\":\"my inc1\"}," +
		"{\"Name\":\"test2\",\"TaxNumber\":22222222,\"Phone\":\"22222222\",\"Address\":\"addres2\",\"OwnerName\":\"my inc2\"}]"
	etalonListCompanies := []dto.CompanyDto{
		{
			Name:      "test1",
			TaxNumber: 1111111,
			Phone:     "1111111",
			Address:   "addres1",
			OwnerName: "my inc1",
		},
		{
			Name:      "test2",
			TaxNumber: 22222222,
			Phone:     "22222222",
			Address:   "addres2",
			OwnerName: "my inc2",
		},
	}

	jh := JsonHandlerImplementation{}

	listCompanies, err := jh.ConvertToCompanyDto([]byte(jsonListCompanies))
	if err != nil {
		t.Error(err)
	}

	if listCompanies[0] != etalonListCompanies[0] || listCompanies[1] != etalonListCompanies[1] {
		t.Error("Wrong conversion result")
	}
}
