package tools

import (
	"companiesListRetranslator/dto"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetCurrentCompanies(t *testing.T) {
	t.Parallel()

	etalonListCompanies := []dto.CompanyDto{
		{
			Name:      "test1",
			TaxNumber: 1111111,
			Phone:     "1111111",
			Address:   "addres1",
			OwnerName: "my inc1",
		},
		{
			Name:      "test2",
			TaxNumber: 22222222,
			Phone:     "22222222",
			Address:   "addres2",
			OwnerName: "my inc2",
		},
	}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		fmt.Fprintln(w, `[{"Name":"test1","TaxNumber":1111111,"Phone":"1111111","Address":"addres1","OwnerName":"my inc1"},
		{"Name":"test2","TaxNumber":22222222,"Phone":"22222222","Address":"addres2","OwnerName":"my inc2"}]`)
	}))
	defer ts.Close()

	config := dto.ConfigDto{
		Urlapi: ts.URL,
	}
	hh := HttpHandlerImplementation{JsonConvertor: &JsonHandlerImplementation{}}

	companies, err := hh.GetCurrentCompanies(config)
	if err != nil {
		t.Error()
	}

	t.Deadline()

	if etalonListCompanies[0] != companies[0] || etalonListCompanies[1] != companies[1] {
		t.Error("Wrong api result")
	}
}
