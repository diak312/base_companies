package tools

import (
	"encoding/json"

	"companiesListRetranslator/dto"
)

type JsonHandlerImplementation struct {
}

func (jsonHandler *JsonHandlerImplementation) ConvertToCompanyDto(bytesCompanies []byte) ([]dto.CompanyDto, error) {
	var companies []dto.CompanyDto
	err := json.Unmarshal(bytesCompanies, &companies)
	if err != nil {
		return []dto.CompanyDto{}, err
	}

	return companies, nil
}
