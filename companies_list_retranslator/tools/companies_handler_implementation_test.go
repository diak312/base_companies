package tools

import (
	"companiesListRetranslator/dto"
	"testing"
)

func TestCompareCompanies(t *testing.T) {
	t.Parallel()

	ch := CompaniesHandlerImplementation{}
	requestedCompanies := getRequestedCompanies()
	dbCompanies := getDbCompanies()
	etalon := getEtalon()

	comparisonResult, err := ch.CompareCompanies(requestedCompanies, dbCompanies)
	if err != nil {
		t.Error(err)
	}

	if comparisonResult.AddableCompanies[0] != etalon.AddableCompanies[0] ||
		comparisonResult.DeletableCompanies[0] != etalon.DeletableCompanies[0] ||
		comparisonResult.DeletableCompanies[1] != etalon.DeletableCompanies[1] ||
		len(comparisonResult.DeletableCompanies) != 2 {
		t.Error("Wrong comparison result")
	}
}

func getEtalon() dto.CompaniesHandlerDto {
	return dto.CompaniesHandlerDto{
		AddableCompanies: []dto.CompanyDto{
			{
				Name:      "test3",
				TaxNumber: 333,
				Phone:     "phone3",
				Address:   "address3",
				OwnerName: "owner3",
			},
		},
		DeletableCompanies: []int{1, 2},
	}
}

func getRequestedCompanies() []dto.CompanyDto {
	return []dto.CompanyDto{
		{
			Name:      "test3",
			TaxNumber: 333,
			Phone:     "phone3",
			Address:   "address3",
			OwnerName: "owner3",
		},
		{
			Name:      "test4",
			TaxNumber: 444,
			Phone:     "phone4",
			Address:   "address4",
			OwnerName: "owner4",
		},
	}
}

func getDbCompanies() []dto.CompanyDto {
	return []dto.CompanyDto{
		{
			Id:        1,
			Name:      "test1",
			TaxNumber: 111,
			Phone:     "phone1",
			Address:   "address1",
			OwnerName: "owner1",
		},
		{
			Id:        2,
			Name:      "test2",
			TaxNumber: 222,
			Phone:     "phone2",
			Address:   "address2",
			OwnerName: "owner2",
		},
		{
			Id:        4,
			Name:      "test4",
			TaxNumber: 444,
			Phone:     "phone4",
			Address:   "address4",
			OwnerName: "owner4",
		},
	}
}
