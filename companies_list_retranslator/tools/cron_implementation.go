package tools

import (
	"fmt"
	"time"

	"companiesListRetranslator/dto"
	"companiesListRetranslator/repositories"
)

type CronImplementation struct {
	HttpInterrogator HttpHandler
	Repository       repositories.CompaniesRepository
	CompaniesHandler CompaniesHandler
}

func (cronTool *CronImplementation) Init(config dto.ConfigDto) {
	for {
		go cronTool.updateCompanies(config)
		time.Sleep(time.Duration(config.Requestfrequencyapi) * time.Second)
	}
}

func (cronTool *CronImplementation) updateCompanies(config dto.ConfigDto) {
	requestedCompanies, err := cronTool.HttpInterrogator.GetCurrentCompanies(config)
	if err != nil {
		fmt.Println(err)
	}

	dbCompanies, err := cronTool.Repository.GetList()
	if err != nil {
		fmt.Println(err)
	}

	comparationResult, err := cronTool.CompaniesHandler.CompareCompanies(requestedCompanies, dbCompanies)

	if len(comparationResult.AddableCompanies) != 0 {
		err := cronTool.Repository.AddList(comparationResult.AddableCompanies)
		if err != nil {
			fmt.Println(err)
		}
	}

	if len(comparationResult.DeletableCompanies) != 0 {
		err := cronTool.Repository.DeleteByListId(comparationResult.DeletableCompanies)
		if err != nil {
			fmt.Println(err)
		}
	}
}
