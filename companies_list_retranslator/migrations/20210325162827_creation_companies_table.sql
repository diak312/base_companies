-- +goose Up
CREATE TABLE IF NOT EXISTS companies
(
	id SERIAL NOT NULL
		CONSTRAINT companies_pk
			PRIMARY KEY,
	name_company TEXT,
	tax_number BIGINT,
	phone TEXT,
	address TEXT,
	owner TEXT
);
-- +goose StatementBegin
SELECT 'up SQL query';
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
