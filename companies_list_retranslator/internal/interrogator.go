package bootstrap

import (
	"companiesListRetranslator/tools"
)

type Interrogator struct {
}

func (interrogator *Interrogator) Init() error {
	config, err := InitConfig()
	if err != nil {
		return err
	}

	err = InitMigrations(config)
	if err != nil {
		return err
	}

	companiesRepository, err := GetCompaniesRepository(config)
	if err != nil {
		return err
	}

	httpHandler := tools.HttpHandlerImplementation{JsonConvertor: &tools.JsonHandlerImplementation{}}
	companiesHandler := tools.CompaniesHandlerImplementation{}

	cron := tools.CronImplementation{
		HttpInterrogator: &httpHandler,
		Repository:       companiesRepository,
		CompaniesHandler: &companiesHandler,
	}
	cron.Init(config)

	return nil
}
