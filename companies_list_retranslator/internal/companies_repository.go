package bootstrap

import (
	"companiesListRetranslator/dto"
	"companiesListRetranslator/repositories"
)

func GetCompaniesRepository(config dto.ConfigDto) (*repositories.CompaniesRepositoryPgsql, error) {
	dhInitialiser := DBInitialiser{}
	err := dhInitialiser.Init(config)

	if err != nil {
		return nil, err
	}

	poolConnections := dhInitialiser.GetPool()

	return &repositories.CompaniesRepositoryPgsql{poolConnections}, nil
}
