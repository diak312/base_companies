package bootstrap

import (
	"companiesListRetranslator/dto"
	"os"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

func InitConfig() (dto.ConfigDto, error) {
	prodPath := "/build/.env"
	devPath := ".env.dev"
	var config dto.ConfigDto

	if _, err := os.Stat(prodPath); err == nil || os.IsExist(err) {
		if err := godotenv.Load(prodPath); err != nil {
			return dto.ConfigDto{}, err
		}
	} else if _, err := os.Stat(devPath); err == nil || os.IsExist(err) {
		if err := godotenv.Load(devPath); err != nil {
			return dto.ConfigDto{}, err
		}
	} else {
		return dto.ConfigDto{}, err
	}

	err := envconfig.Process("companiesRetranslator", &config)

	if err != nil {
		return dto.ConfigDto{}, err
	}

	return config, nil
}
