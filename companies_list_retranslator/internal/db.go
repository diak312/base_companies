package bootstrap

import (
	"context"
	"fmt"

	"companiesListRetranslator/dto"

	"github.com/jackc/pgx/v4/pgxpool"
)

type DBInitialiser struct {
	pool *pgxpool.Pool
}

func (initialiser *DBInitialiser) Init(config dto.ConfigDto) error {
	pool, err := pgxpool.Connect(context.Background(), config.Dbconnection)

	if err != nil {
		return err
	}

	fmt.Println("Pool connection database got!")

	initialiser.pool = pool

	return nil
}

func (initialiser *DBInitialiser) GetPool() *pgxpool.Pool {
	return initialiser.pool
}
