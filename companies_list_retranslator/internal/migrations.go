package bootstrap

import (
	"companiesListRetranslator/dto"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/pressly/goose"
)

func InitMigrations(config dto.ConfigDto) error {
	db, err := goose.OpenDBWithDriver("postgres", config.Dbmigrationsconnection)
	if err != nil {
		return err
	}

	defer func() {
		if err := db.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	if err := goose.Run("up", db, "migrations"); err != nil {
		return err
	}

	return nil
}
