module companiesListRetranslator

go 1.16

require (
	github.com/jackc/pgx/v4 v4.10.1
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.3.0
	github.com/pressly/goose v2.7.0+incompatible
)
